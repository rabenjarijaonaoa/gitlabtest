/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import gitlab.Calculator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inclusiv 11
 */
public class TestUnit {

    public TestUnit() {
    }

    Calculator calculator = new Calculator();

    @Test
    public void testAdd() {
        int result = calculator.add(5, 3);
        assertEquals(8, result);
    }

    @Test
    public void testSubtract() {
        int result = calculator.subtract(10, 4);
        assertEquals(6, result);
    }

    @Test
    public void testDivide() {
        double result = calculator.divide(20.0, 4.0);
        assertEquals(5.0, result, 0.001);
    }

    @Test
    public void testMultiply() {
        int result = calculator.multiply(6, 7);
        assertEquals(42, result);
    }
}
